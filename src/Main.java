
public class Main {
    public static void main(String[] args) {

        SomeNode n = new SomeNode(1, new SomeNode(2, new SomeNode(3, new SomeNode(4, new SomeNode("in", null)))));

        System.out.println(reverseNode(n));
    }

    private static Object reverseNode(SomeNode n) {
        SomeNode result = SomeNode.reverseHelper(n);
        return result.first;
    }
}
