class SomeNode<T> {

    SomeNode next;
    T first;

    SomeNode(T first, SomeNode next) {
        this.first = first;
        this.next = next;
    }

    static SomeNode reverseHelper(SomeNode n) {
        SomeNode second = n.next;
        SomeNode third = second.next;

        n.next = null;
        second.next = n;

        SomeNode current = third;
        SomeNode previous = second;

        while (current != null) {
            SomeNode next = current.next;
            current.next = previous;

            previous = current;
            current = next;
        }

        return previous;
    }

}